## LDOCE5から音声を抽出してAnkiデッキ（CSV）に追加するツール

### Usage

    usage: attach-ldoce5-voice [-h] --ldoce5 LDOCE5 [--country {us,gb}]
                               [--column COLUMN]
                               [--delimiter {comma,tab,space,semicolon}]
                               input output
    
    This program extracts pronunciation files from LDOCE5 and attachs it to a csv
    file.
    
    positional arguments:
      input                 path to a input csv file
      output                path to a output csv file; !!pay attention to that
                            this file will be overwritten!!
    
    optional arguments:
      -h, --help            show this help message and exit
      --ldoce5 LDOCE5       path to 'ldoce5.data' directory; e.g. 'C:\Program
                            Files\Longman\LDOCE5\ldoce5.data'
      --country {us,gb}     the country code of pronunciation files; default is
                            'us'
      --column COLUMN       the index number of column in which contents are
                            outputted; default is '2'
      --delimiter {comma,tab,space,semicolon}
                            the delimiter of the csv file; default is 'comma'

### Example

#### Move to a directory contains attach-ldoce5-voice.exe and csv files at first.
    > cd /d C:\Users\rubyu\Downloads\attach-ldoce5-voice-20131002
    > dir /B
    attach-ldoce5-voice.exe
    SVL-01.csv
    SVL-02.csv
    ...

#### Here we'll use a single column csv file.
    > type SVL-01.csv
    "a"
    "able"
    ...

#### Attempt to generate new csv file.
    > attach-ldoce5-voice.exe --ldoce5 "C:\Program Files\Longman\LDOCE5\ldoce5.data" SVL-01.csv SVL-01v.csv > SVL-01.csv.log

#### Two column csv file has been generated,
    > type SVL-01v.csv
    "a", "[sound:SVL-01v-0.mp3]"
    "able", "[sound:SVL-01v-1.mp3]"
    ...

#### and pronunciation files have also been generated.
    > dir /B SVL-01v.media
    SVL-01v-0.mp3
    SVL-01v-1.mp3
    ...

#### The log should be the following.
    > type SVL-01.csv.log
    ==========
    LDOCE5 directory: C:\Program Files\Longman\LDOCE5\ldoce5.data
    country code: us
    input csv file: SVL-01.csv
    output csv file: SVL-01v.csv
    csv delimiter: comma
    csv column index: 2
    ==========
    media dir is not exists; Creating... done.
    Scanning files... done.
    
    1: a ... OK
      entry id: 114ee8912f5.-2d2c
     >>  1: [a] prio=0, xpath=Head//HWD//BASE
    
    2: able ... OK
      entry id: 114ee8912f5.-2951
     >>  1: [able] prio=0, xpath=Head//HWD//BASE
    
    ...
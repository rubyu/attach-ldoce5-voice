# -*- coding: utf-8 -*-
'''
Created on 2013/09/25

@author: user
'''

import logging
import string
import unicodedata
from itertools import chain, ifilter
from xml.etree import ElementTree
from ldoce5viewer.ldoce5.idmreader import list_files, ArchiveReader


class WordVariation(object):
    @classmethod
    def get(cls, word):
        if not isinstance(word, unicode):
            word = unicode(word)
        mutate_functions = [cls._remove_accents,
                            cls._remove_punctuation,
                            cls._remove_whitespace,
                            cls._lower,
                            cls._strip,
                            cls._remove_last_s,]
        total = [word]
        current = [word]
        new = []
        while True:
            for w in current:
                for f in mutate_functions:
                    _w = f(w)
                    if _w not in total:
                        total.append(_w)
                        new.append(_w)
                        yield _w
            if len(new) == 0:
                break
            current = new
            new = []
                 
    @classmethod
    def _remove_accents(cls, word):
        return cls._ascii_filter(unicodedata.normalize("NFKD", word)) 
    
    @classmethod
    def _remove_punctuation(cls, word):
        return filter(lambda c: c not in string.punctuation, word)
    
    @classmethod
    def _remove_whitespace(cls, word):
        return filter(lambda c: c not in string.whitespace, word)
    
    @classmethod
    def _ascii_filter(cls, word):
        return filter(lambda c: c in string.printable, word)
        
    @classmethod
    def _lower(cls, word):
        return word.lower()
    
    @classmethod
    def _strip(cls, word):
        return word.strip()
    
    @classmethod
    def _remove_last_s(cls, word):
        return cls._remove_last(word, "s")
    
    @classmethod
    def _remove_last(cls, word, text):
        if word.endswith(text):
            return word[:-len(text)]
        else:
            return word


class Voice(object):
    """
    
    Entry
        Head
                HWD            BASE,INFLX
                ORTHVAR        BASE,INFLX
                Variant        BASE,INFLX                    analytic
                Variant        ORTHVAR        BASE,INFLX     
                AmEVariant     BASE,INFLX                    valor
                Inflections    PLURALFORM     span.tail      yourselves
                
                
        Sense   
                LEXVAR         span.tail                     diabolic
        Tail     
                DERIV          BASE,INFLX                    zoological
                AmEVariant     ORTHVAR        BASE,INFLX     tranquility
        
    # 単数・複数なども厳密考えて、
    # 見出し語、英米での表記の揺れ、だけに対応して、あとは無視したほうがよさげ
    Entry
        Head
                HWD                 BASE                          head word
                AmEVariant          ORTHVAR        BASE           neighbor
                AmEVariant          BASE                          aluminum
                Variant(has GEO)    BASE                          whiskey
                Variant(hasn't GEO) BASE                          caddie

                
                
    #todo 表記の揺れに厳密に対応
    #  NG among -> amonguest
    #  NG num -> number
    #  OK whisky -> whiskey    GEOタグあり
    #  OK caddie -> caddy      GEOタグなし
    #  
    #  GEOタグでは判別できない…が、とりあえずゴミが入らない方向で
    
    #todo 同一の見出しの場合は複数音声を出力 
    #  close1, close2, close3 で、2,3の音が一緒…
    #  
    #  →これは、Weblioで調べましょう的な解決かなぁ
    
    #todo 音声についての詳細を追加する
    """
    
    def __init__(self, ldoce5_dir=None, country_code=None):
        self._max_index_key = 4
        self._keys = xrange(self._max_index_key+1)
        self._initialize_index()
        if ldoce5_dir and country_code:
            self.scan(ldoce5_dir, country_code)
    
    def scan(self, ldoce5_dir, country_code):
        self._pron_archive_name = self._pron_archive_name(country_code)
        self._pron_reader = ArchiveReader(ldoce5_dir, self._pron_archive_name)
        self._entry_reader = ArchiveReader(ldoce5_dir, "fs")
        self._scan_entries(ldoce5_dir)
        
    def _initialize_index(self):
        self._index = []
        for _ in xrange(self._max_index_key+1):
            self._index.append({})
    
    def _scan_entries(self, ldoce5_dir):
        self._initialize_index()
        prons = self._scan_prons(ldoce5_dir)
        for (entry_location, entry) in self._entry(ldoce5_dir):
            paths = list(self._pron_path(entry))
            safe_paths = list(ifilter(lambda x: x in prons, paths))
            
            for path in set(paths) - set(safe_paths):
                logging.debug("missing: %s/%s", self._pron_archive_name, path)
                    
            if len(safe_paths) == 0:
                continue
            
            pron_location = prons[safe_paths[0]]
            
            def register(prio, xpath, word):
                self._register(prio, word, (entry_location, prio, xpath, pron_location))
            
            for (prio, xpath, word) in self._word(entry):
                _word = unicode(word)
                register(prio, xpath, _word)
                for variation in WordVariation.get(_word):
                    register(self._max_index_key, xpath, variation)                    
            
    def _pron_path(self, entry):
        for audio in entry.findall(".//Audio"):
            resource = audio.get("resource").lower().strip()
            path = audio.get("topic")
            if resource != self._pron_archive_name:
                continue
            yield path
    
    def _entry(self, ldoce5_dir):
        for (_, _, location) in list_files(ldoce5_dir, "fs"):
            entry = ElementTree.fromstring(self._entry_reader.read(location))
            yield (location, entry)
    
    def _extract_node(self, element, xpath):
        for elm in element.findall(xpath):
            yield elm
                            
    def _extract_node_text(self, element, xpath):
        for elm in self._extract_node(element, xpath):
            yield elm.text

    def _word(self, entry):
        for prio, xpath in ((0, "Head//HWD//BASE"),
                            (1, "Head//AmEVariant//ORTHVAR//BASE"),
                            (2, "Head//AmEVariant//BASE"),
                            (3, "Head//Variant/GEO/..//BASE"),
                            ):
            for word in self._extract_node_text(entry, xpath):
                yield(prio, xpath, word)
    
    @classmethod
    def _pron_archive_name(cls, country_code):
        if country_code == "us":
            return "us_hwd_pron"
        elif country_code == "gb":
            return "gb_hwd_pron"
    
    def _scan_prons(self, ldoce5_dir):
        prons = {}
        for (dirs, name, location) in list_files(ldoce5_dir, self._pron_archive_name):
            path = "/".join(dirs) + "/" + name
            prons[path] = location
        return prons
    
    def _register(self, key, word, data):
        for key in xrange(key):
            if word in self._index[key]:
                return
        if not word in self._index[key]:
            self._index[key][word] = []
        self._index[key][word].append(data)
 
    def candidates(self, word):
        if not isinstance(word, unicode):
            word = unicode(word)
        for variation in chain([word], WordVariation.get(word)):
            for key in self._keys:
                if variation in self._index[key]:
                    for (e_loc, prio, xpath, p_loc) in self._index[key][variation]:
                        yield (e_loc, prio, xpath, variation, p_loc)

    def get(self, word):
        if not isinstance(word, unicode):
            word = unicode(word)
        for (_, _, _, _, p_loc) in self.candidates(word):
            return self._pron_reader.read(p_loc)


class Entry(object):
    def __init__(self, ldoce5_dir=None):
        self._initialize_index()
        if ldoce5_dir:
            self.scan(ldoce5_dir)
    
    def scan(self, ldoce5_dir):
        self._reader = ArchiveReader(ldoce5_dir, "fs")
        self._scan_entries(ldoce5_dir)
        
    def _initialize_index(self):
        self._index = {}
    
    def _scan_entries(self, ldoce5_dir):
        self._initialize_index()
        for (location, id_) in self._entry_info(ldoce5_dir):
            self._index[id_] = location
                
    def _entry_info(self, ldoce5_dir):
        for (_, _, location) in list_files(ldoce5_dir, "fs"):
            entry = ElementTree.fromstring(self._reader.read(location))
            id_ = entry.get("id")
            if id_:
                yield (location, self._normalize_id(id_))
        
    @classmethod
    def _normalize_id(cls, id_):
        return ".".join(id_.split(".")[2:])
        
    def get_location(self, id_):
        if id_ in self._index:
            return self._index[id_]
        
    def get(self, id_):
        location = self.get_location(id_)
        if location:
            return self._reader.read(location)
        
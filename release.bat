del attach-ldoce5-voice-*.zip
rmdir /s /q dist
rmdir /s /q attach-ldoce5-voice
python setup.py py2exe
ping localhost -n 5 > nul
xcopy dist attach-ldoce5-voice\
"C:\Program Files\7-Zip\7z.exe" a attach-ldoce5-voice-%date:~-10,4%%date:~-5,2%%date:~-2,2%.zip attach-ldoce5-voice
pause

# -*- coding: utf-8 -*-
#
# setup.py

'''
Created on 2013/02/28

@author: user
'''

from distutils.core import setup
import py2exe

setup(data_files=["readme.txt"], 
      console=[{"script": "attach_ldoce5_voice.py",
                "dest_base": "attach-ldoce5-voice"}],
      options={ "py2exe": {"compressed": 1,
                           "optimize": 2,
                           "bundle_files": 1}},
      zipfile=None)
# -*- coding: utf-8 -*-
'''
Created on 2013/09/11

@author: user
'''

import logging
import locale
import codecs
import sys
import os
import csv
import zlib
from argparse import ArgumentParser
from ldoce5viewer.ldoce5.idmreader import is_ldoce5_dir
from ldoce5 import Voice, Entry, WordVariation
from xml.etree import ElementTree


logging.basicConfig(level=logging.INFO)
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout, 
                                                             errors="xmlcharrefreplace")


def get_arg():
    parser = ArgumentParser(prog="attach-ldoce5-voice",
                            description="""This program extracts pronunciation files """
                            """from LDOCE5 and attachs it to a csv file.""")
    parser.add_argument("--ldoce5", 
                        required=True,
                        help="""path to 'ldoce5.data' directory; """
                        """e.g. 'C:\Program Files\Longman\LDOCE5\ldoce5.data'""")
    parser.add_argument("--country", 
                        choices=["us", "gb"], 
                        default="us",
                        help="""the country code of pronunciation files; """
                        """default is 'us'""")
    parser.add_argument("--column", 
                        type=int, 
                        default=2,
                        help="""the index number of column in which contents are outputted; """
                        """default is '2'""")
    parser.add_argument("--delimiter", 
                        choices=["comma", "tab", "space", "semicolon"], 
                        default="comma",
                        help="""the delimiter of the csv file; """
                        """default is 'comma'""")
    parser.add_argument("input", 
                        help="""path to a input csv file""")
    parser.add_argument("output", 
                        help="""path to a output csv file; """
                        """!!pay attention to that this file will be overwritten!!""")
    return parser.parse_args()

def delimiter_character(arg):
    if arg.delimiter == "comma":
        return ","
    elif arg.delimiter == "tab":
        return "\t"
    elif arg.delimiter == "space":
        return " "
    elif arg.delimiter == "semicolon":
        return ";"
    
def write_media_data(path, data):
    f = open(path, "wb")
    f.write(data)

def print_headers(arg):
    print "=" * 10    
    print "LDOCE5 directory: %s" % arg.ldoce5
    print "country code: %s" % arg.country
    print "input csv file: %s" % arg.input
    print "output csv file: %s" % arg.output
    print "csv delimiter: %s" % arg.delimiter
    print "csv column index: %s" % arg.column
    print "=" * 10    

def print_result(voice, word):
    (e_loc, _, _, found, _) = voice.candidates(word).next()
    entry = ElementTree.fromstring(voice._entry_reader.read(e_loc))
    id_ = Entry._normalize_id(entry.get("id"))
    print "  entry id: %s" % id_
    for i, (prio, xpath, word_) in enumerate(voice._word(entry)):
        if found == word_ or \
        found in set(WordVariation.get(word_)):
            print " >> ",
        else:
            print "  | ",
        print "%s: [%s] prio=%s, xpath=%s" % (i+1, word_, prio, xpath) 

def main():
    arg = get_arg()
    media_dir = os.path.splitext(arg.output)[0] + ".media"
    media_prefix = os.path.splitext(os.path.split(arg.output)[1])[0]
    
    print_headers(arg)

    if not is_ldoce5_dir(arg.ldoce5):
        sys.stderr.white("%s is not LDOCE5 directory")
        return
    
    if not os.path.exists(media_dir):
        print "media dir is not exists; Creating...",
        os.mkdir(media_dir)
        print "done."
    
    print "Scanning files...",
    voice = Voice(arg.ldoce5, arg.country)
    print "done."
    with open(arg.input, "rb") as old, \
    open(arg.output, "wb") as new:
        old_reader = csv.reader(old, delimiter=delimiter_character(arg))
        new_writer = csv.writer(new, delimiter=delimiter_character(arg), quoting=csv.QUOTE_ALL)
        for i, row in enumerate(old_reader):
            voice_data = None
            media_file = os.path.join(media_dir, "%s-%s.mp3" % (media_prefix, i))
            media_file_name = os.path.split(media_file)[1]
            row.extend([""] * (arg.column - len(row)))
            word = row[0]
            print
            print "%s: %s ..." % (i+1, word),
            try:
                voice_data = voice.get(word)
            except zlib.error:
                print "NG; error when decoding voice data; this may be broken"
            else:
                if voice_data:
                    write_media_data(media_file, voice_data)
                    row[arg.column-1] = row[arg.column-1] + "[sound:%s]" % media_file_name
                    print "OK"
                    print_result(voice, word)
                else:
                    print "NG; voice data not found"
            new_writer.writerow(row)
    print
    print "complete."
    
if __name__ == "__main__":
    main()
    
# -*- coding: utf-8 -*-
'''
Created on 2013/09/23

@author: user
'''

import logging
logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)
import unittest

if __name__ == "__main__":
    suite = unittest.TestLoader().discover("test", pattern = "*_test.py")
    unittest.TextTestRunner(verbosity=2).run(suite)
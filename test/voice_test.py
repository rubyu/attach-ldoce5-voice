# -*- coding: utf-8 -*-
'''
Created on 2013/09/11

@author: user
'''

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)


import unittest
from ldoce5 import Voice


class TestVoice(unittest.TestCase):
    def test_pron_archive_name(self):
        for country_code, pron_archive_name in (("us", "us_hwd_pron"), 
                                                ("gb", "gb_hwd_pron")):
            res = Voice._pron_archive_name(country_code)
            self.assertEqual(res, pron_archive_name)
            
# -*- coding: utf-8 -*-
'''
Created on 2013/09/11

@author: user
'''

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)


import unittest
from ldoce5 import WordVariation


class TestWordVariation(unittest.TestCase):
    def test_remove_accents(self):
        for data, answer in ((u"café", u"cafe"),):
            res = WordVariation._remove_accents(data)
            self.assertEqual(res, answer)
            
    def test_get_variations(self):
        for (word, answer) in ((u"café", [u"cafe"]),
                               (u"Miss", [u"miss", u"Mis", u"mis", u"Mi", u"mi"]),
                               (u"home room", [u"homeroom"]),
                               (u"Jeep", [u"jeep"]),
                               ):
            res = WordVariation.get(word)
            self.assertEqual(list(res), answer)
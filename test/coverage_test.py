# -*- coding: utf-8 -*-
'''
Created on 2013/09/11

@author: user
'''

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)


import unittest
import csv
import zlib
from ldoce5 import Voice

class TestCoverage(unittest.TestCase):
    def test_coverage(self):
        """
DEBUG    coverage_test    test_coverage   @52 - not found(344): ['beautifully', 'dancing', 'eighteenth', 'eightieth', 'fifteenth', 'fiftieth', 'fortieth', 'fourteenth', 'greatness', 'nineteenth', 'ninetieth', 'ought', 'seventeenth', 'seventieth', 'sixtieth', 'speaking', 'thirteenth', 'thirtieth', 'twentieth', 'yourselves', 'according', 'babysitter', 'brightly', 'calmly', 'differently', 'hundredth', 'silently', 'sketchbook', 'softly', 'tightly', 'waiting', 'actively', 'agricultural', 'asking', 'correctly', 'finishing', 'proudly', 'secretly', 'sooner', 'thousandth', 'undoubtedly', 'willingly', 'accurately', 'approximately', 'arched', 'bravely', 'carelessly', 'conqueror', 'eagerly', 'eagerness', 'emotionally', 'generously', 'golfer', 'hardness', 'independently', 'loneliness', 'loosely', 'mentally', 'millionth', 'patiently', 'researcher', 'rudely', 'salespeople', 'separately', 'solidly', 'steadily', 'tenderly', 'unexpectedly', 'accidentally', 'architectural', 'bitterness', 'boldness', 'breaking', 'brightness', 'Buddhist', 'calmness', 'casually', 'cheaply', 'cheapness', 'clearness', 'continually', 'continuously', 'coolness', 'cowardly', 'curiously', 'delicately', 'dishonestly', 'displease', 'dissatisfy', 'dullness', 'earnestly', 'financially', 'idleness', 'impossibility', 'intended', 'interruption', 'modestly', 'neatly', 'noiseless', 'noisily', 'peacefully', 'quietness', 'sharpness', 'simultaneously', 'solemnly', 'stillness', 'storytelling', 'sweetly', 'teaspoonful', 'tenderness', 'unhappiness', 'universally', 'weakly', 'whiteness', 'abandonment', 'campaigner', 'descendent', 'diligence', 'dining', 'distorted', 'fasting', 'furnished', 'ironical', 'operating', 'planetary', 'punctuality', 'rationality', 'rehabilitation', 'shining', 'sociologist', 'statistical', 'suppression', 'temporarily', 'unchanged', 'watering', 'accumulation', 'acknowledgment', 'affectionately', 'alternatively', 'analytic', 'ancestral', 'biographical', 'cleverly', 'conflicting', 'congressional', 'consolidation', 'cruelly', 'designing', 'disobedience', 'divided', 'domination', 'elegance', 'eloquence', 'emigration', 'emphatically', 'eruption', 'estimated', 'fading', 'faintly', 'formulation', 'friendliness', 'genuinely', 'habitually', 'horrified', 'hurrah', 'illustrated', 'impeachment', 'implied', 'industrialize', 'keenly', 'liberation', 'organizational', 'paradoxical', 'permanence', 'persecution', 'pleasantly', 'profoundly', 'rapidity', 'recognizable', 'recruitment', 'redefine', 'redesign', 'reduced', 'relevance', 'removed', 'sophistication', 'stimulation', 'sufficiently', 'urgency', 'absurdity', 'adequacy', 'affirmation', 'alternately', 'behavioral', 'congestion', 'contamination', 'discoverer', 'distortion', 'egoist', 'egoistic', 'elaboration', 'endorsement', 'enrichment', 'evacuation', 'exceeding', 'extravagance', 'flashing', 'geological', 'gratification', 'homosexuality', 'humanist', 'idly', 'illiteracy', 'imperfection', 'inefficiency', 'insecurity', 'irrigation', 'legitimacy', 'loading', 'manipulation', 'objectivity', 'patriotism', 'recreational', 'rediscover', 'reenter', 'reestablish', 'reluctantly', 'respectfully', 'sampling', 'severity', 'shaped', 'sliding', 'sociological', 'spontaneity', 'unsold', 'utopian', 'validity', 'willingness', 'accountability', 'affluence', 'anthropologist', 'archaeologist', 'attempted', 'bacterial', 'briskly', 'caddie', 'catastrophic', 'centralization', 'clothespin', 'compression', 'concealment', 'discernible', 'egotistic', 'endangered', 'fineness', 'firing', 'imperialist', 'inauguration', 'interrogation', 'lawmaking', 'magnificence', 'mediation', 'minutely', 'modernist', 'molecular', 'overcrowd', 'performing', 'presumable', 'relational', 'screaming', 'sparing', 'timidity', 'tranquility', 'zoological', 'adjoining', 'alluring', 'amply', 'autocratic', 'boomer', 'choreographer', 'contrition', 'convergence', 'cynicism', 'defection', 'demolition', 'deportation', 'deterioration', 'devaluation', 'disintegration', 'eligibility', 'enactment', 'entrench', 'extradition', 'forcing', 'fragmentation', 'grudging', 'idiosyncratic', 'impoverished', 'infringement', 'inoculation', 'intestinal', 'pleading', 'procurement', 'respectability', 'skinner', 'stagnation', 'theological', 'verification', 'vindication', 'abdominal', 'acquiescence', 'aggregation', 'annexation', 'atheist', 'benevolence', 'corroboration', 'delineation', 'depletion', 'dermatologist', 'despotic', 'destitution', 'deterrence', 'diabolic', 'discomfiture', 'distal', 'divergent', 'eviction', 'exhortation', 'exorcise', 'feasibility', 'frugality', 'futility', 'hedonism', 'inadvertent', 'intoxication', 'juxtaposition', 'ostracism', 'overrate', 'ratification', 'rebuttal', 'sacrilegious', 'seclude', 'vantage', 'ventilation']
DEBUG    coverage_test    test_coverage   @53 - broken(7): ['wonderful', 'wood', 'would', 'wonder', 'wooden', 'woo', 'wooded']
----------------------------------------------------------------------
Ran 1 test in 75.110s
       """
        
        ldoce5_dir = r"Y:\rubyu\code\anki_deck_builder\voice\ldoce5.data"
        test_file = r"Y:\rubyu\code\anki_deck_builder\voice\py\test\coverage.csv"
        country_code = "us"
        voice = Voice(ldoce5_dir, country_code)
        
        found = []
        notfound = []
        broken = []
        
        for row in csv.reader(open(test_file)):
            word = row[0]
            voice_data = None
            try:
                voice_data = voice.get(word)
            except zlib.error:
                broken.append(word)
            else:
                if voice_data:
                    found.append(word)
                else:
                    notfound.append(word)
        
        logging.debug("found(%s): %s", len(found), found)
        logging.debug("not found(%s): %s", len(notfound), notfound)
        logging.debug("broken(%s): %s", len(broken), broken)
        
# -*- coding: utf-8 -*-
'''
Created on 2013/09/11

@author: user
'''

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)

import os
import sys
import unittest
from attach_ldoce5_voice import main

class TestMain(unittest.TestCase):
    def test_output(self):
        ldoce5_dir = r"Y:\rubyu\code\anki_deck_builder\voice\ldoce5.data"
        test_dir = r"Y:\rubyu\code\anki_deck_builder\voice\py\test\tmp_main_test"
        
        test_input_file = os.path.join(test_dir, "test_in.csv")
        test_output_file = os.path.join(test_dir, "test_out.csv")
        
        import shutil
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)
        os.mkdir(test_dir)
        with open(test_input_file, "w") as f:
            f.write("abandon\n")
            f.write("abnormal\n")
            f.write("wonderful\n")
            f.write("cafe\n")
            f.write("colorful\n")
            f.write("colourful\n")
            f.write("neighbour\n")
            f.write("neighbor\n")
            f.write("zoological\n")
            f.write("zoology\n")
        sys.argv = ["", 
                    "--ldoce5", ldoce5_dir, 
                    "--country", "us", 
                    test_input_file, 
                    test_output_file]
        main()
        
# -*- coding: utf-8 -*-
'''
Created on 2013/09/11

@author: user
'''

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)


import unittest
from ldoce5 import Entry

class TestEntry(unittest.TestCase):
    """
    ['ought', 'storytelling', 'descendent', 'redefine', 'alternately', 'behavioral', 
    'rediscover', 'reestablish', 'unsold', 'clothespin', 'fineness', 'lawmaking', 
    'presumable', 'relational', 'boomer', 'convergence', 'distal']
    """
    def test_normalize_id(self):
        res = Entry._normalize_id("u2fc098491a42200a.6e2b450a.114ee8912f5.-2d7f")
        self.assertEqual(res, "114ee8912f5.-2d7f")
    
    def test_get(self):
        ldoce5_dir = r"Y:\rubyu\code\anki_deck_builder\voice\ldoce5.data"
        entry = Entry(ldoce5_dir)
        
        for id_ in ("11503730847.5f44", #cafe -> café
                    "1150415cba4.75b7", #miss -> Miss 1
                    "1150415cba4.7652", #miss -> Miss 2
                    "11504595e24.-5d0f", #toward -> towards
                    "114ee8912f5.3a0", #afterward -> afterwards
                    "11503f8ca60.-6c4b", #homeroom -> home room
                    "1150433a334.43b9", #ought -> ought to
                    "115045a11b6.-6dc7", #whiskey -> whisky
                    "115045a11b6.-284f", #yourselves -> yourself
                    "1150373ac67.-3ff", #coffeepot -> coffee pot
                    "11503f8ca60.3ea4", #jeep -> Jeep
                    "1150373ac67.-1373", #clothespin -> clothes peg
                    "1150415cba4.52c7", #Messrs. -> Messrs
                    "11504595e24.2a22", #valor -> valour
                    "114ee8912f5.7d04", #boomer -> baby boomer
                    "1150373ac67.-675a", #centennial -> centenary
                    "115045a11b6.-284f", #yourselves -> yourself
                    "115037e1a3c.8f", #diabolic -> diabolical
                    "11504595e24.-5029", #tranquility -> tranquil
                    "115045a11b6.-6dc7", #whisky -> whiskey
                    ):
            text = entry.get(id_)
            logging.debug("%s: %s", id_, text)
